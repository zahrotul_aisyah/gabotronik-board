/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
#include <asf.h>
#include "main.h"
#include <string.h>
#include "math.h"
#include <stdio.h>
#include <util/delay.h>

// adc - function initialization
static void adc_handler(ADC_t *adc, uint8_t ch_mask, adc_result_t result);
static void adc_init(void);

// adc - variable to handle the conversion result
static int8_t battery_voltage;
int ac_voltage[80];
uint8_t n_buff=0;


/**
 * \brief ADC interrupt callback handler.
 */
static void adc_handler(ADC_t *adc, uint8_t ch_mask, adc_result_t result)
{

	
	switch (ch_mask)
	{
	case ADC_CH0:
		battery_voltage = result;
		break;
	case ADC_CH1:
		// save voltage value and squared voltage value to buffer
		
		
		if(n_buff<80)
		{
			ac_voltage[n_buff] = result;
			n_buff++;
		}
		else
		{
			
		}
		break;
		
	default:
		break;
	}
}

/**
 * \brief ADC initialitation function.
 */
static void adc_init(void)
{
	struct adc_config adc_conf;
	struct adc_channel_config adcch_conf;
	
	adc_read_configuration(&ADCA, &adc_conf);
	adcch_read_configuration(&ADCA, ADC_CH0, &adcch_conf);
	adcch_read_configuration(&ADCA, ADC_CH1, &adcch_conf);
	
	// configure the adc module
	// set conversion parameters to signed, 8-bit, with external analog reference on PORTA
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_8, ADC_REF_AREFA);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_FREERUN_SWEEP, 2, 0);
	adc_set_clock_rate(&adc_conf, 5000UL);
	adc_set_callback(&ADCA, &adc_handler);
	adc_write_configuration(&ADCA, &adc_conf);
	
	// configure the adc channel for battery voltage measurement
	// interrupt mode, ADC6 - ADC5 (CH1 Op-Amp output - 1V reference)
	// check the xprotolab schematic to make sure of it
	adcch_enable_interrupt(&adcch_conf);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN6, ADCCH_NEG_PIN5, 1);
	adcch_write_configuration(&ADCA, ADC_CH0, &adcch_conf);
	
	// configure the adc channel for AC voltage measurement
	// interrupt mode, ADC6 - ADC5 (CH1 Op-Amp output - 1V reference)
	// check the xprotolab schematic to make sure of it
	//adcch_enable_interrupt(&adcch_conf);
	adcch_enable_interrupt(&adcch_conf);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN7, ADCCH_NEG_PIN5, 1);
	adcch_write_configuration(&ADCA, ADC_CH1, &adcch_conf);
}


int main (void)
{
	// uart - configuration structure
	static usart_serial_options_t usart_options = {
		.baudrate	= USART_SERIAL_BAUDRATE,
		.charlength = USART_SERIAL_CHAR_LENGTH,
		.paritytype = USART_SERIAL_PARITY,
		.stopbits	= USART_SERIAL_STOP_BIT
	};
	
	// uart - variables for command
	uint8_t received_char;
	char command_string[64];
	char *command_buf = command_string;
	bool buf_full = false;
	
	
	
	bool start_summing;
	float ac_rms;
	static uint8_t in_zero_cross, out_zero_cross;
	static uint8_t sample_no=0;
	static float ac_squared_sum=0;
	static uint8_t temp_n_buff;
	
	
	// initialize system
	sysclk_init();
	board_init();
	pmic_init();
	cpu_irq_enable();
	
	// serial - initialize
	usart_serial_init(USART_SERIAL, &usart_options);
	
	// adc - initialize
	adc_init();
	adc_enable(&ADCA);
	
	
	for(;;)
	{
		if (buf_full == false) {
			usart_serial_getchar(USART_SERIAL, &received_char);
			*command_buf = received_char;
			
			if (*command_buf == '\r' || *command_buf == '\n') {
				*command_buf = '\0';
				command_buf = command_string;
				buf_full = true;
				} else {
				command_buf++;
			}
		}
		
		if (buf_full == true) 
		{
			if (strcmp("read_ch1", command_string) == 0) 
			{
				// convert the adc value to voltage using this equation:
				//voltage = 0.1575661125*adc_result + 0.1345665631
				float ch1_voltage;
				char ch1_string[32];
				ch1_voltage = 0.1575661125*battery_voltage + 0.1345665631;
				_delay_ms(100);
				sprintf(ch1_string, "%.2f", ch1_voltage);
				usart_serial_write_packet(USART_SERIAL, ch1_string, strlen(ch1_string));
			}
			else if(strcmp("read_ch2", command_string) == 0)
			{
				start_summing = false;
				if(n_buff==80)
				{
					for (sample_no=1; sample_no<80; sample_no++)
					{
						if (ac_voltage[sample_no-1] < 0 && ac_voltage[sample_no] >= 0) //zerocross
						{
							if (start_summing)
							{
								out_zero_cross=sample_no;
								start_summing=false;
							}
							else
							{
								in_zero_cross=sample_no;
								start_summing=true;
							}
						}
						if (start_summing)
						{
							ac_squared_sum=ac_squared_sum+((0.1575661125*ac_voltage[sample_no] + 0.1345665631)*(0.1575661125*ac_voltage[sample_no] + 0.1345665631));
						}
						
					}
				
					ac_rms = (sqrtf(ac_squared_sum/ (out_zero_cross-in_zero_cross)))*209.71;
					char ch2_string[32];
					_delay_ms(100);
					sprintf(ch2_string,"%.2f", ac_rms);
					usart_serial_write_packet(USART_SERIAL, ch2_string, strlen(ch2_string));
				
					ac_squared_sum=0;
					ac_rms=0;
				
					n_buff=0;	
					
				}
				else
				{
					
					temp_n_buff = n_buff;
				}
				
				
			}
			else
			{
				usart_serial_write_packet(USART_SERIAL, "Invalid command!\n\r", strlen("Invalid command!\n\r"));
			}
			buf_full = false;
		}
		
	}
		
}
